package de.abas.extnd;


import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.common.type.IdImpl;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.purchasing.RequestEditor;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;

@EventHandler(head = RequestEditor.class, row = RequestEditor.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class RequestEventHandler {
	
	VendorHandler vendorHandler = new VendorHandler();
	private static final String REQUEST_LOCKED_CUSTOMER = "Request locked vendor";
	
	@FieldEventHandler(field="vendor", type = FieldEventType.VALIDATION)
	public void vendorValidation(FieldEvent event, ScreenControl screenControl, DbContext ctx, RequestEditor head){
		new VendorHandler(ctx,head.getVendor()).checkGeneralLockStatus(ctx);
	}
	
	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, RequestEditor head)throws EventException{
		handleVendorOrderLockSituation(ctx, head);
	}
	
	private void handleVendorOrderLockSituation(DbContext ctx, RequestEditor head) throws EventException {
		if(isVendorOrderLocked(ctx, head))
			throw new EventException(REQUEST_LOCKED_CUSTOMER);
	}
	
	private boolean isVendorOrderLocked(DbContext ctx, RequestEditor head) {
		return isValidVendor(head) && getVendorObject(ctx, head).getYextndnoorder();
	}
	
	private boolean isValidVendor(RequestEditor head) {
		return head.getVendor() != null;
	}
	
	private Vendor getVendorObject(DbContext ctx, RequestEditor head) {
		return ctx.load(Vendor.class, new IdImpl(head.getVendor().getId().toString()));
	}

}
