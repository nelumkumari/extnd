package de.abas.extnd;

import java.util.ArrayList;
import java.util.List;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.common.type.IdImpl;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.Query;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.SelectableCustomer;
import de.abas.erp.db.selection.ExpertSelection;
import de.abas.erp.db.selection.Selection;

public class CustomerHandler {

	private static final String SEPERATOR = " -- ";
	private static final String OFFER_LOCKED_CUSTOMER = "Offer Locked Customer";
	private static final String GENERAL_LOCKED_CUSTOMER = "General Locked Customer";
	private static final String WARNING = "Warning !";
	Customer customer;
	
	public CustomerHandler(DbContext ctx,SelectableCustomer selectableCustomer){
		customer = ctx.load(Customer.class, new IdImpl(selectableCustomer.getId().toString()));
	}
	
	
	public CustomerHandler(){}
	
	private boolean isValidCustomer() {
		return customer != null;
	}
	
	public Customer getCustomerObject(DbContext ctx, SelectableCustomer selectableCustomer){
		return ctx.load(Customer.class, new IdImpl(selectableCustomer.getId().toString()));
	}
	
	public boolean isGeneralLock(){
		return isValidCustomer() && customer.getYextndgenlock(); 
	}
	
	
	public boolean isOfferLock(){
		return isValidCustomer() && customer.getYextndnooffer(); 
	}
	
	
	public boolean isDeliveryLock(){
		return isValidCustomer() && customer.getYextndnodelivery(); 
	}

		
	public boolean isCreditLimitCheckingForOfferTicked(){
		return isValidCustomer() && customer.getYextndcloffer(); 
	}
	
	
	public boolean isCreditLimitCheckingForOrderTicked(){
		return isValidCustomer() && customer.getYextndclorder();
	}
	
	
	public boolean isCreditLimitCheckingForDeliveryrTicked(){
		return isValidCustomer() && customer.getYextndcldelivery();
	}
	
	
	public boolean isOutstandingItemsTicked(){
		return isValidCustomer() && customer.getYextndoutstanding();
	}
	
	
	public boolean isOpenOrdersTicked(){
		return isValidCustomer() && customer.getYextndopenorders();
	}
	
	
	public boolean checkGeneralLockStatus(DbContext ctx){
		if(isGeneralLock())
			showMessage(ctx,GENERAL_LOCKED_CUSTOMER);
		return true;
	}

	
	public boolean checkOfferlLockStatus(DbContext ctx){
		if(isOfferLock())
			showMessage(ctx,OFFER_LOCKED_CUSTOMER);
		return true;
	}

	
	public boolean checkDeliverylLockStatus(){
		return isDeliveryLock();
	}

	
	public double getCreditLimit(){
		return isValidCustomer() ? customer.getCredLim().doubleValue() : 0.0;
	}
	
	private void showMessage(DbContext ctx,String messageBody){
		new TextBox(ctx, WARNING, messageBody).show();
	}
	

	
	public List<String> getListofCustomersByField(DbContext ctx, String field, String value, String nummer){
		return getCustomerList(getQuery(ctx, field, value, nummer));
	}


	private List<String> getCustomerList(Query<Customer> query) {
		List<String> customers = new ArrayList<>();
		for (Customer matchedCustomer : query)
			customers.add(matchedCustomer.getIdno() + SEPERATOR + matchedCustomer.getAddr());
		return customers;
	}


	private Query<Customer> getQuery(DbContext ctx, String field, String value, String nummer) {
		String criteria = "@group=1;idno<>" + nummer + ";swd=;" + field + "~" + value + ";@language=en" ;
		Selection<Customer> selection = ExpertSelection.create(Customer.class,criteria);
		return ctx.createQuery(selection);
	}
		
}
