package de.abas.extnd;

import java.util.ArrayList;
import java.util.List;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.common.type.IdImpl;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.Query;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.SelectableCustomer;
import de.abas.erp.db.schema.vendor.SelectableVendor;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.selection.ExpertSelection;
import de.abas.erp.db.selection.Selection;


public class VendorHandler {
	
	private static final String SEPERATOR = " -- ";
	private static final String ORDER_LOCKED_VENDOR = "Order Locked Vendor";
	private static final String REQUEST_LOCKED_VENDOR  = "Request Locked vendor";
	private static final String GENERAL_LOCKED_VENDOR  = "General Locked vendor";
	private static final String WARNING = "Warning !";
	Vendor vendor;
	
	public VendorHandler(DbContext ctx,SelectableVendor selectableVender){
		vendor = ctx.load(Vendor.class, new IdImpl(selectableVender.getId().toString()));
	}
	
	
	public VendorHandler(){}
	
	private boolean isValidVendor() {
		return vendor != null;
	}
	
	public Customer getCustomerObject(DbContext ctx, SelectableCustomer selectableCustomer){
		return ctx.load(Customer.class, new IdImpl(selectableCustomer.getId().toString()));
	}
	
	public boolean isGeneralLock(){
		return isValidVendor() && vendor.getYextndgenlock(); 
	}
	
	
	public boolean isRequestLock(){
		return isValidVendor() && vendor.getYextndnorequest(); 
	}
	
	
	public boolean isOrderLock(){
		return isValidVendor() && vendor.getYextndnoorder(); 
	}
	
	
	public boolean checkGeneralLockStatus(DbContext ctx){
		if(isGeneralLock())
			showMessage(ctx,GENERAL_LOCKED_VENDOR);
		return true;
	}

	
	public boolean checkRequestLockStatus(DbContext ctx){
		if(isRequestLock())
			showMessage(ctx,REQUEST_LOCKED_VENDOR);
		return true;
	}
	
	public boolean checkOrderLockStatus(DbContext ctx){
		if(isRequestLock())
			showMessage(ctx,ORDER_LOCKED_VENDOR);
		return true;
	}

	
	public boolean checkOrderlLockStatus(){
		return isOrderLock();
	}

	
	private void showMessage(DbContext ctx,String messageBody){
		new TextBox(ctx, WARNING, messageBody).show();
	}
	
	
	public List<String> getListofVendorsByField(DbContext ctx, String field, String value, String nummer){
		return getVendorList(getQuery(ctx, field, value, nummer));
	}


	private List<String> getVendorList(Query<Vendor> query) {
		List<String> vendors = new ArrayList<>();
		for (Vendor matchedVendor : query)
			vendors.add(matchedVendor.getIdno() + SEPERATOR + matchedVendor.getAddr());
		return vendors;
	}


	private Query<Vendor> getQuery(DbContext ctx, String field, String value, String nummer) {
		String criteria = "@group=1;idno<>" + nummer + ";swd=;" + field + "~" + value + ";@language=en" ;
		Selection<Vendor> selection = ExpertSelection.create(Vendor.class,criteria);
		return ctx.createQuery(selection);
	}
		
	
	
}
