package de.abas.extnd;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.common.type.IdImpl;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.sales.PackingSlipEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;

@EventHandler(head = PackingSlipEditor.class, row = PackingSlipEditor.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class PackingSlipEventHandler {
	
	private static final String DELIVERY_LOCKED_CUSTOEMR = "Delivery locked customer";
	private static final String CREDIT_LIMIT_LOCK_IS_MANUALLY_RELEASED = "Credit Limit Lock is manually released";
	private static final String CREDIT_LIMIT_LOCK = "Credit Limit Lock";


	@FieldEventHandler(field="customer", type = FieldEventType.VALIDATION)
	public void customerValidation(FieldEvent event, ScreenControl screenControl, DbContext ctx, PackingSlipEditor head){
		handleGeneralLockSituation(ctx, head);
	}


	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, PackingSlipEditor head) throws EventException{
		handleDeliveryLockSituation(ctx, head);
		handleCustomerCreditLimit( ctx, head); 
		unlockTransferManually(head);
	}

	@FieldEventHandler(field="yextndclmanually", type = FieldEventType.EXIT)
	public void yextndclmanuallyExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, PackingSlipEditor head){
		if(head.getYextndclmanually())
			new TextBox(ctx,CREDIT_LIMIT_LOCK,CREDIT_LIMIT_LOCK_IS_MANUALLY_RELEASED).show();	
	}
	
	private void handleGeneralLockSituation(DbContext ctx, PackingSlipEditor head) {
		if(isValidCustomer(head))
			new CustomerHandler(ctx,head.getCustomer()).checkGeneralLockStatus(ctx);	
	}

	private boolean isValidCustomer(PackingSlipEditor head) {
		return head.getCustomer() != null;
	}
	
	private void unlockTransferManually(PackingSlipEditor head) {
		if(head.getYextndclmanually())
			head.setTransLock(false);	
	}

	private void handleCustomerCreditLimit(DbContext ctx, PackingSlipEditor head) {
		if(isLocked(head) && isCreditLimitReached(ctx, head)){
			ctx.out().print("This should handle later - get field name from Patric");	
		}
	}

	private boolean isCreditLimitReached(DbContext ctx, PackingSlipEditor head) {
		return new CreditLimitCheck(getCustomerObject(ctx, head),ctx,head,0).checkCustomerCreditLimit();
	}

	private Customer getCustomerObject(DbContext ctx, PackingSlipEditor head) {
		return ctx.load(Customer.class, new IdImpl(head.getCustomer().getId().toString()));
	}

	private boolean isLocked(PackingSlipEditor head) {
		return isValidCustomer(head) && !head.getYextndclmanually();
	}

	private void handleDeliveryLockSituation(DbContext ctx, PackingSlipEditor head) throws EventException {
		if(new CustomerHandler(ctx,head.getCustomer()).checkDeliverylLockStatus())
			throw new EventException(DELIVERY_LOCKED_CUSTOEMR);
	}

	@FieldEventHandler(field="customer", type = FieldEventType.EXIT)
	public void customerExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, PackingSlipEditor head){
		if(isValidCustomer(head))
			checkCreditLimit(ctx, head);
		else
			unlockDelivery(head);		
	}

	private void checkCreditLimit(DbContext ctx, PackingSlipEditor head) {
		new CreditLimitCheck(getCustomerObject(ctx, head),ctx,head,0).checkCustomerCreditLimit();
	}

	private void unlockDelivery(PackingSlipEditor head) {
		head.setTransLock(false);
		head.setYextndcreditlock(false);
	}

	

}
