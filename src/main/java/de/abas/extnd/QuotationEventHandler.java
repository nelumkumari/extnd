package de.abas.extnd;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.common.type.IdImpl;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.sales.QuotationEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.axi.event.EventException;

@EventHandler(head = QuotationEditor.class, row = QuotationEditor.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class QuotationEventHandler {
	private static final String OFFERS_LOCKED_CUSTOMER = "Offers locked customer";
	private static final String MSGBOX_TITLE = "Credit Limit Lock";
	private static final String MSGBOX_BODY = "Credit Limit Lock is manually released";
	
	@FieldEventHandler(field="customer", type = FieldEventType.EXIT)
	public void customerExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, QuotationEditor head){
		handleCustomerCreditLimit(ctx, head);
	}
	
	@FieldEventHandler(field="yextndclmanually", type = FieldEventType.EXIT)
	public void yextndclmanuallyExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, QuotationEditor head){
		if(head.getYextndclmanually())
			new TextBox(ctx,MSGBOX_TITLE,MSGBOX_BODY).show();	
		
	}
	
	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, QuotationEditor head) throws EventException{
		handleCustomerOfferLockSituation(ctx, head);
		setTransferLock(head);
	}
	
	
	@FieldEventHandler(field="customer", type = FieldEventType.VALIDATION)
	public void customerValidation(FieldEvent event, ScreenControl screenControl, DbContext ctx, QuotationEditor head) {
		handleCustomerGeneralLockSituation(ctx, head);
	}
	
	
	private void handleCustomerCreditLimit(DbContext ctx, QuotationEditor head) {
		if(isValidCustomer(head))
			checkCustomerCreditLimit(ctx, head);
		else
			unlockQuotation(head);	
		
	}

	private void unlockQuotation(QuotationEditor head) {
		head.setTransLock(false);
		head.setYextndcreditlock(false);
	}

	private void checkCustomerCreditLimit(DbContext ctx, QuotationEditor head) {
		new CreditLimitCheck(getCustomerObject(ctx, head),ctx,head,0).checkCustomerCreditLimit();
	}
	

	private void setTransferLock(QuotationEditor head) {
		if(head.getYextndclmanually())
			head.setTransLock(false);	
		
	}
	
	private void handleCustomerOfferLockSituation(DbContext ctx, QuotationEditor head) throws EventException {
		if(isCustomerOfferLocked(ctx, head))
			throw new EventException(OFFERS_LOCKED_CUSTOMER);
	}

	private boolean isCustomerOfferLocked(DbContext ctx, QuotationEditor head) {
		return isValidCustomer(head) && getCustomerObject(ctx, head).getYextndnooffer();
	}
	
	private boolean isValidCustomer(QuotationEditor head) {
		return head.getCustomer() != null;
	}
	
	private Customer getCustomerObject(DbContext ctx, QuotationEditor head) {
		return ctx.load(Customer.class, new IdImpl(head.getCustomer().getId().toString()));
	}
	

	private void handleCustomerGeneralLockSituation(DbContext ctx, QuotationEditor head) {
		if(isValidCustomer(head))
			new CustomerHandler(ctx,head.getCustomer()).checkGeneralLockStatus(ctx);
		
	}

}
