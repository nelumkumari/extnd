package de.abas.extnd;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.common.type.enums.EnumSalesProcess;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.referencetypes.PurchasingAndSalesProcessEditor;
import de.abas.erp.db.schema.sales.InvoiceEditor;
import de.abas.erp.db.schema.sales.QuotationEditor;
import de.abas.erp.db.schema.sales.SalesOrderEditor;



public class CreditLimitCheck {
	
	
	private static final String CREDIT_LIMIT_LOCK_IS_RELEASED = "Credit limit lock is released";
	CustomerHandler customerHandler;
	Customer customer;
	DbContext ctx;
	PurchasingAndSalesProcessEditor salesHeader;
	private double totalOfOpenOrders = 0d;
	private double customerCreditLimit = 0d;
	private double totalOfOutstandings = 0d;
	
	private static final String TEXT_NEW_LINE = "\n";
	private static final String TEXT_EQUAL = " = ";
	private static final String MSGBOX_TITLE = "Credit limit information";
	private static final String MSGBOX_CREDIT_LIMIT_REACHED = "Credit limit reached";
	private static final String CUSTOMER_CREDIT_LIMIT = "Customer Credit Limit ";
	private static final String TOTAL_OPEN_ORDERS = "Total Open Orders";
	private static final String TOTAL_OUTSTANDINGS = "Total Outstandings";
	double currentOrderAmount =0d;
	
	public CreditLimitCheck(Customer customer,DbContext ctx,PurchasingAndSalesProcessEditor salesHeader,double currentOrderAmount){
		this.customer = customer;
		this.ctx = ctx;
		this.customerHandler = new CustomerHandler(ctx, customer);
		this.salesHeader = salesHeader;
		this.totalOfOpenOrders = new OpenOrders(customer,ctx).getOpenOrdersTotalValue();
		this.totalOfOutstandings = new Outstandings(customer).getOutstanfing();
		this.customerCreditLimit = customerHandler.getCreditLimit();
		this.currentOrderAmount = currentOrderAmount;
	}
	
	
	
	public boolean checkCustomerCreditLimit(){
		return processCreditLimitReached();
	}
	
	
	private boolean processCreditLimitReached(){
		if(checkAgainstOpenOrders()){
			lockUnlockSalesObject(true);
			showCreditLimitReachedMessageBox(TOTAL_OPEN_ORDERS, (totalOfOpenOrders + currentOrderAmount) );
			return true;
		}else if(checkAgainstOutstandingItems()){
			lockUnlockSalesObject(true);
			showCreditLimitReachedMessageBox(TOTAL_OUTSTANDINGS, totalOfOutstandings );
			return true;
		}else{
			showCreditLimitLockReleasedMessageBox();
			lockUnlockSalesObject(false);
			return false;
		}
	}



	private boolean checkAgainstOutstandingItems() {
		return customerHandler.isCreditLimitCheckingForOfferTicked() && customerHandler.isOutstandingItemsTicked() && totalOfOutstandings > customerCreditLimit;
	}



	private boolean checkAgainstOpenOrders() {
		return customerHandler.isCreditLimitCheckingForOfferTicked() && customerHandler.isOpenOrdersTicked() && (totalOfOpenOrders + currentOrderAmount) > customerCreditLimit;
	}
	

	
	private void lockUnlockSalesObject(boolean lockState){
		EnumSalesProcess salesType = salesHeader.getEnum(EnumSalesProcess.class, "type");
		switch(salesType){
			case Quotation:
				lockQuotation(lockState);
				break;
			case SalesOrder:
				lockSalesOrder(lockState);
				break;
			case Invoice:
				lockSalesInvoice(lockState);
				break;
			default:
					
		}
	}



	private void lockSalesInvoice(boolean lockState) {
		((InvoiceEditor)salesHeader).setTransLock(lockState);
		((InvoiceEditor)salesHeader).setYextndcreditlock(lockState);	
	}



	private void lockSalesOrder(boolean lockState) {
		((SalesOrderEditor)salesHeader).setTransLock(lockState);
		((SalesOrderEditor)salesHeader).setYextndcreditlock(lockState);
	}



	private void lockQuotation(boolean lockState) {
		((QuotationEditor)salesHeader).setTransLock(lockState);
		((QuotationEditor)salesHeader).setYextndcreditlock(lockState);
	}
	
	

	private void showCreditLimitReachedMessageBox(String type, double calculatedValue){
		new TextBox(ctx, MSGBOX_TITLE, MSGBOX_CREDIT_LIMIT_REACHED + TEXT_NEW_LINE + TEXT_NEW_LINE + type + TEXT_EQUAL + String.format("%.2f", calculatedValue) +  TEXT_NEW_LINE + CUSTOMER_CREDIT_LIMIT + TEXT_EQUAL  + customerCreditLimit).show();	
	}
	
	private void showCreditLimitLockReleasedMessageBox(){
		new TextBox(ctx, MSGBOX_TITLE, CREDIT_LIMIT_LOCK_IS_RELEASED).show();
	}
	

}
