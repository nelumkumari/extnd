package de.abas.extnd;

import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.standard.vk.OutstandingSalesOrders;
import de.abas.erp.db.schema.customer.Customer;

public class OpenOrders {
	
	Customer customer;
	DbContext ctx;
	
	public OpenOrders(Customer customer,DbContext ctx){
		this.customer = customer;
		this.ctx = ctx;
	}
	
	public double getOpenOrdersTotalValue() {
		try(OutstandingSalesOrders outstandingSalesOrders = ctx.openInfosystem(OutstandingSalesOrders.class)){
			outstandingSalesOrders.setKkuli(customer);
			outstandingSalesOrders.invokeStart();
			return outstandingSalesOrders.getSummeohne().doubleValue();
		}		
	}
}
