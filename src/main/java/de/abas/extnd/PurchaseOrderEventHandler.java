package de.abas.extnd;

import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.common.type.IdImpl;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.purchasing.PurchaseOrderEditor;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;

@EventHandler(head = PurchaseOrderEditor.class, row = PurchaseOrderEditor.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class PurchaseOrderEventHandler {

	VendorHandler vendorHandler; 
	private static final String ORDERS_LOCKED_CUSTOMER = "Orders locked vendor";
	
	@FieldEventHandler(field="vendor", type = FieldEventType.VALIDATION)
	public void vendorValidation(FieldEvent event, ScreenControl screenControl, DbContext ctx, PurchaseOrderEditor head){
		new VendorHandler(ctx,head.getVendor()).checkGeneralLockStatus(ctx);
	}
	
	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, PurchaseOrderEditor head)throws EventException{
		handleVendorOrderLockSituation(ctx, head);
	}
	
	private void handleVendorOrderLockSituation(DbContext ctx, PurchaseOrderEditor head) throws EventException {
		if(isVendorOrderLocked(ctx, head))
			throw new EventException(ORDERS_LOCKED_CUSTOMER);
	}
	
	private boolean isVendorOrderLocked(DbContext ctx, PurchaseOrderEditor head) {
		return isValidVendor(head) && getVendorObject(ctx, head).getYextndnoorder();
	}
	
	private boolean isValidVendor(PurchaseOrderEditor head) {
		return head.getVendor() != null;
	}
	
	private Vendor getVendorObject(DbContext ctx, PurchaseOrderEditor head) {
		return ctx.load(Vendor.class, new IdImpl(head.getVendor().getId().toString()));
	}
	
}
