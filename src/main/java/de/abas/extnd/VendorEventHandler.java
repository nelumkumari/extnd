package de.abas.extnd;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.company.CompanyData;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.schema.vendor.VendorEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.type.FieldEventType;

@EventHandler(head = VendorEditor.class)

@RunFopWith(EventHandlerRunner.class)

public class VendorEventHandler {
	
	private static final String FOUND = " found ";
	private static final String TEXT_DUPLICATE_VENDOR = "Duplicate vendor ";
	private static final String NEW_LINE = "\n";
	private Map<String,String> fieldMap = new HashMap<>();

	CompanyDataHandler companyDataHandler = new CompanyDataHandler();
	VendorHandler vendorHandler;

	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, VendorEditor head) throws EventException {
		initilizeVendorHandler();
		validateAllFields(ctx, head);
	}
		

	@FieldEventHandler(field="yextndnoorder", type = FieldEventType.EXIT)
	public void yextndnoorderExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, VendorEditor head){
		if(head.getYextndnoorder())
			head.setYextndnorequest(false);	
	}
	
	@FieldEventHandler(field="yextndnorequest", type = FieldEventType.EXIT)
	public void yextndnorequestExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, VendorEditor head){
		if(head.getYextndnorequest())
			head.setYextndnoorder(false);
	}
	
	

	private void validateAllFields(DbContext ctx, VendorEditor head) throws EventException {
		createFieldMapping();
		for (HashMap.Entry<String, String> field : fieldMap.entrySet())
			handleVendorField(ctx,head,field.getKey(),field.getValue());
	}


	private void initilizeVendorHandler() {
		vendorHandler =  new VendorHandler();
	}

	
	private void handleVendorField(DbContext ctx, VendorEditor head,String englishFieldName, String fieldInCompanyData) throws EventException {
		if(isValidVendorFieldCheck(ctx, head,englishFieldName,fieldInCompanyData))
			checkVendorFieldDuplicationR(ctx, head,englishFieldName,head.getString(englishFieldName));
	}
	
	private boolean isValidVendorFieldCheck(DbContext ctx, VendorEditor head,String fieldName,String fieldInCompanyData) {
		return companyDataHandler.getBooleanFieldValue(ctx,fieldInCompanyData) && !head.getString(fieldName).isEmpty();
	}
	
	
	private void checkVendorFieldDuplicationR(DbContext ctx, VendorEditor head,String englishFieldName, String fieldValue) throws EventException {
		provideErrorMessage(getMessageR(vendorHandler.getListofVendorsByField(ctx, englishFieldName, fieldValue,head.getIdno()),head.getFieldMeta(englishFieldName).getGermanName()));
	}
	
	
	private String getMessageR(List<String> matchingobjects,String fieldMeaning){
		if(hasMatchingObjects(matchingobjects))
			return TEXT_DUPLICATE_VENDOR + fieldMeaning + FOUND + NEW_LINE + NEW_LINE + String.join(NEW_LINE, matchingobjects);
		else
			return null;
	}
	

	private void provideErrorMessage(String error) throws EventException {
		if(hasMessage(error))
			throw new EventException(error);
	}

	private boolean hasMessage(String error) {
		return error != null;
	}

	private boolean hasMatchingObjects(List<String> matchingobjects) {
		return !matchingobjects.isEmpty();
	}

	
	
	private void createFieldMapping() {
		fieldMap.put(Vendor.META.addr.getName(),CompanyData.META.yextndsname.getName());
		fieldMap.put(Vendor.META.street.getName(), CompanyData.META.yextndsstreet.getName());
		fieldMap.put(Vendor.META.phoneNo.getName(),  CompanyData.META.yextndstel.getName());
		fieldMap.put(Vendor.META.webSiteURL.getName(),  CompanyData.META.yextndcurl.getName());
	}


}
