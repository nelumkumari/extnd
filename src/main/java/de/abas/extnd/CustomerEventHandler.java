package de.abas.extnd;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.company.CompanyData;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.CustomerEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.type.FieldEventType;

@EventHandler(head = CustomerEditor.class)

@RunFopWith(EventHandlerRunner.class)

public class CustomerEventHandler {
	
	private static final String FOUND = " found ";
	private static final String DUPLICATE_CUSTOMER = "Duplicate customer ";
	private static final String NEW_LINE = "\n";

	CompanyDataHandler companyDataHandler = new CompanyDataHandler();
	CustomerHandler customerHandler;
	private Map<String,String> fieldMap = new HashMap<>();

	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head) throws EventException {
		initilizeCustomerHandler();
		validateAllFields(ctx, head);
		String s = head.getYextndrep2().toString();
		
	}
	
	
	@FieldEventHandler(field="yextndoutstanding", type = FieldEventType.EXIT)
	public void yextndoutstandingExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head){
		if(head.getYextndoutstanding())
			head.setYextndopenorders(false);	
	}


	@FieldEventHandler(field="yextndopenorders", type = FieldEventType.EXIT)
	public void yextndopenordersExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head) {
		if(head.getYextndopenorders())
			head.setYextndoutstanding(false);
	}
	
	private void initilizeCustomerHandler() {
		customerHandler =  new CustomerHandler();
	}
	
	
	
	private void validateAllFields(DbContext ctx, CustomerEditor head) throws EventException {
		createFieldMapping();
		for (HashMap.Entry<String, String> field : fieldMap.entrySet())
			handleCustomerField(ctx,head,field.getKey(),field.getValue());
	}


	
	private void handleCustomerField(DbContext ctx, CustomerEditor head,String englishFieldName, String fieldInCompanyData) throws EventException {
		if(isValidCustomerFieldCheck(ctx, head,englishFieldName,fieldInCompanyData))
			checkCustomerFieldDuplicationR(ctx, head,englishFieldName,head.getString(englishFieldName));
	}
	
	private boolean isValidCustomerFieldCheck(DbContext ctx, CustomerEditor head,String fieldName,String fieldInCompanyData) {
		return companyDataHandler.getBooleanFieldValue(ctx,fieldInCompanyData) && !head.getString(fieldName).isEmpty();
	}
	
	
	private void checkCustomerFieldDuplicationR(DbContext ctx, CustomerEditor head,String englishFieldName, String fieldValue) throws EventException {
		provideErrorMessage(getMessageR(customerHandler.getListofCustomersByField(ctx, englishFieldName, fieldValue,head.getIdno()),head.getFieldMeta(englishFieldName).getGermanName()));
	}
	
	
	private String getMessageR(List<String> matchingobjects,String fieldMeaning){
		if(hasMatchingObjects(matchingobjects))
			return DUPLICATE_CUSTOMER + fieldMeaning + FOUND + NEW_LINE + NEW_LINE + String.join(NEW_LINE, matchingobjects);
		else
			return null;
	}
	
	

	private void provideErrorMessage(String error) throws EventException {
		if(hasMessage(error))
			throw new EventException(error);
	}

	private boolean hasMessage(String error) {
		return error != null;
	}



	private boolean hasMatchingObjects(List<String> matchingobjects) {
		return !matchingobjects.isEmpty();
	}


	
	private void createFieldMapping() {
		fieldMap.put(Customer.META.addr.getName(),CompanyData.META.yextndsname.getName());
		fieldMap.put(Customer.META.street.getName(), CompanyData.META.yextndsstreet.getName());
		fieldMap.put(Customer.META.phoneNo.getName(),  CompanyData.META.yextndstel.getName());
		fieldMap.put(Customer.META.webSiteURL.getName(),  CompanyData.META.yextndcurl.getName());
	}
	

}
