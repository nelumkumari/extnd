package de.abas.extnd;

import de.abas.erp.api.gui.TextBox;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.sales.SalesOrderEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.common.type.IdImpl;

@EventHandler(head = SalesOrderEditor.class, row = SalesOrderEditor.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class SalesOrderEventHandler {
	
	private static final String CREDIT_LIMIT_LOCK_IS_MANUALLY_RELEASED = "Credit Limit Lock is manually released";
	private static final String CREDIT_LIMIT_LOCK = "Credit Limit Lock";
	CustomerHandler customerHandler;
	Customer customer;
	

	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, SalesOrderEditor head){
		handleCustomerCreditLimit(event, ctx, head);
		unlockSalesOrderManually(head);
	}

	@FieldEventHandler(field="customer", type = FieldEventType.EXIT)
	public void customerExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, SalesOrderEditor head){
		if(isValidCustomer(head))
			handleCustomerCreditLimit(ctx, head);
		else
			unlockSalesOrder(head);	
		
	}

	
	@FieldEventHandler(field="yextndclmanually", type = FieldEventType.EXIT)
	public void yextndclmanuallyExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, SalesOrderEditor head){
		if(head.getYextndclmanually())
			new TextBox(ctx,CREDIT_LIMIT_LOCK,CREDIT_LIMIT_LOCK_IS_MANUALLY_RELEASED).show();	
	}

	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, SalesOrderEditor head){
		head.setYextndclmanually(false);
	}
	
	
	
	private boolean isValidCustomer(SalesOrderEditor head) {
		return head.getCustomer() != null;
	}

	private void unlockSalesOrder(SalesOrderEditor head) {
		head.setTransLock(false);
		head.setYextndcreditlock(false);
	}

	private void handleCustomerCreditLimit(DbContext ctx, SalesOrderEditor head) {
		new CreditLimitCheck(ctx.load(Customer.class, new IdImpl(head.getCustomer().getId().toString())),ctx,head,head.getTotalNetAmt().doubleValue()).checkCustomerCreditLimit();
	}

	
	private void unlockSalesOrderManually(SalesOrderEditor head) {
		if(head.getYextndclmanually())
			head.setTransLock(false);	
	}

	private void handleCustomerCreditLimit(ScreenEvent event, DbContext ctx, SalesOrderEditor head) {
		if(isLocked(head) && isCustomerCreditLimitReached(ctx, head))
				untickScheduling(event, head);
		
	}

	private void untickScheduling(ScreenEvent event, SalesOrderEditor head) {
		if(event.getCommand().getDisplayString().equals("New"))
			loopInRows(head.table().getEditableRows());
	}

	private void loopInRows(Iterable<SalesOrderEditor.Row> rows) {
		for(SalesOrderEditor.Row row : rows)
			setAtRow(row);
	}

	private void setAtRow(SalesOrderEditor.Row row) {
		if(SalesOrderEditor.Row.META.scheduled.isModifiable(row))
			row.setScheduled(false);
	}


	private boolean isLocked(SalesOrderEditor head) {
		return isValidCustomer(head) && !head.getYextndclmanually();
	}

	private boolean isCustomerCreditLimitReached(DbContext ctx, SalesOrderEditor head) {
		return new CreditLimitCheck(ctx.load(Customer.class, new IdImpl(head.getCustomer().getId().toString())),ctx,head,head.getTotalNetAmt().doubleValue()).checkCustomerCreditLimit();
		
	}



}


