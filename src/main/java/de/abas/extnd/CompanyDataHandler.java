package de.abas.extnd;

import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.company.CompanyData;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.settings.DisplayMode;
import de.abas.erp.db.util.QueryUtil;

public class CompanyDataHandler {
	public CompanyData getCompanyData(DbContext ctx){
		ctx.getSettings().setDisplayMode(DisplayMode.DISPLAY);
		SelectionBuilder<CompanyData> selectionBuilder= SelectionBuilder.create(CompanyData.class);
		selectionBuilder.add(Conditions.eq(CompanyData.META.idno, "1"));
		return QueryUtil.getFirst(ctx, selectionBuilder.build());
	}
	
	
	public boolean getBooleanFieldValue(DbContext ctx,String fieldName){
		return getCompanyData(ctx).getString(fieldName).equals("1");
	}
	
	public boolean isCustomerNameTicked(DbContext ctx){
		return getCompanyData(ctx).getYextndcname();
	}
	
	public boolean isCustomerStreetTicked(DbContext ctx){
		return getCompanyData(ctx).getYextndcstreet();
	}
	
	public boolean isCustomerTelephoneTicked(DbContext ctx){
		return getCompanyData(ctx).getYextndctel();
	}
	
	public boolean isCustomerURLTicked(DbContext ctx){
		return getCompanyData(ctx).getYextndcurl();
	}
	
	public boolean isVendorNameTicked(DbContext ctx){
		return getCompanyData(ctx).getYextndsname();
	}
	
	public boolean isVendorStreetTicked(DbContext ctx){
		return getCompanyData(ctx).getYextndsstreet();
	}
	
	public boolean isVendorTelephoneTicked(DbContext ctx){
		return getCompanyData(ctx).getYextndstel();
	}
	
	public boolean isVendorURLTicked(DbContext ctx){
		return getCompanyData(ctx).getYextndsurl();
	}
		
}
