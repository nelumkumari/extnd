package de.abas.extnd;

import de.abas.erp.db.schema.customer.Customer;

public class Outstandings {
	
	Customer customer;
	
	public Outstandings(Customer customer){
		this.customer = customer;
	}
	
	public double getOutstanfing(){
		return customer.getBal().doubleValue();
	}
}
